import numpy as np
import matplotlib.pyplot as plt

N = 100                         # number of nodes (excluding BS)
e = [50, 35, 7, 5, 2, 1]        # prob. that packet is lost at exactly i nodes
c = 100                         # a tuning parameter, choose c = 10 for example
g = []                          # coop. gain if packet is lost at exactly i nodes
G = 0                           # E(g) = expected coop. gain (= expectation of g)
p = 0.1


def h(i):
    return np.tanh(c * i)


def h2(i):
    return i

for i in range(N+1):
    g.append(h2(i) * (1-(1-p)**(N-i)))

print('g(i) = coop. gain if packet is lost at exactly i nodes ==> ')
print('g : {}'.format(g))

#
# for i in range(N+1):
#     G += e[i] * g[i]

print('Expected G = ', G)

# plt.bar(range(N+1), g)        # For bar plot
plt.plot(g)                     # For line graph
plt.ylabel('g')
plt.show()

