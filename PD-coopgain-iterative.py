R = 3
P = 1
T = 5
S = 0


def avg_coop_n_runs(c, d, tft, n):
    if not c:
        return 0
    total_opponents = float(c + d + tft - 1)
    vs_coop = R * ((c-1) / total_opponents)
    vs_def = S * (d / total_opponents)
    vs_tft = R * (tft / total_opponents)
    return vs_coop + vs_def + vs_tft


def avg_defect_n_runs(c, d, tft, n):
    if not d:
        return 0
    total_opponents = float(c + d + tft - 1)
    vs_coop = T * (c / total_opponents)
    vs_def = P * ((d-1) / total_opponents)
    vs_tft = P * (tft / total_opponents) * ((4+n) / n)
    return vs_coop + vs_def + vs_tft


def avg_tft_n_runs(c, d, tft, n):
    if not tft:
        return 0
    total_opponents = float(c + d + tft - 1)
    vs_coop = R * (c / total_opponents)
    vs_def = P * (d / total_opponents) * ((n-1)/float(n))
    vs_tft = R * ((tft-1) / total_opponents)
    return vs_coop + vs_def + vs_tft


def plot_n_runs(c, d, tft, n):
    import matplotlib.pyplot as plt

    font = {'size': 22}
    plt.rc('font', **font)
    plt.rc('text', usetex=True)
    plt.rc('lines', linewidth=2)

    l_avg_coop = []
    l_avg_def  = []
    l_avg_tft = []

    for i in range(1, n+1):
        l_avg_coop.append(avg_coop_n_runs(c, d, tft, i))
        l_avg_def.append(avg_defect_n_runs(c, d, tft, i))
        l_avg_tft.append(avg_tft_n_runs(c, d, tft, i))
    x = range(1, n+1)
    plt.plot(x, l_avg_coop, label="Cooperate")
    plt.plot(x, l_avg_def, label="Defect")
    plt.plot(x, l_avg_tft, label="TFT")
    plt.xlabel("Number of rounds")
    plt.ylabel("Average points")
    plt.legend()
    plt.grid()
    plt.show()


def main():
    c = 10
    d = 10
    tft = 20
    n = 100

    print("Average Points per game for infinite runs for C = {}\t D = {}\t TFT = {}:".format(c, d, tft))

    print("\nAverage Coop   = {}"
          "\nAverage Defect = {}"
          "\nAverage TFT    = {}".format(avg_coop_n_runs(c, d, tft, n),
                                         avg_defect_n_runs(c, d, tft, n),
                                         avg_tft_n_runs(c, d, tft, n)))

if __name__ == '__main__':
    # main()
    plot_n_runs(c=10, d=10, tft=20, n=6)
