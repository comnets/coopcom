R = 3
P = 1
T = 5
S = 0


def avg_coop(c, d, tft):
    if not c:
        return 0
    total_opponents = c + d + tft - 1
    vs_coop = R * ((c-1) / total_opponents)
    vs_def = S * (d / total_opponents)
    vs_tft = R * (tft / total_opponents)
    return vs_coop + vs_def + vs_tft


def avg_defect(c, d, tft):
    if not d:
        return 0
    total_opponents = c + d + tft - 1
    vs_coop = T * (c / total_opponents)
    vs_def = P * ((d-1) / total_opponents)
    vs_tft = P * (tft / total_opponents)
    return vs_coop + vs_def + vs_tft


def avg_tft(c, d, tft):
    if not tft:
        return 0
    total_opponents = c + d + tft - 1
    vs_coop = R * (c / total_opponents)
    vs_def = P * (d / total_opponents)
    vs_tft = R * ((tft-1) / total_opponents)
    return vs_coop + vs_def + vs_tft


def main():
    c = 10
    d = 10
    tft = 20

    print("Average Points per game for infinite runs for C = {}\t D = {}\t TFT = {}:".format(c, d, tft))

    print("\nAverage Coop   = {}"
          "\nAverage Defect = {}"
          "\nAverage TFT    = {}".format(avg_coop(c, d, tft), avg_defect(c, d, tft), avg_tft(c, d, tft)))

if __name__ == '__main__':
    main()
