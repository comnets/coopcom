#! /usr/bin/python

P_cl_r = 1.2    # Watts
P_cl_i = 0.2
P_sr_s = 1.5
P_sr_r = 1.2
P_sr_i = 0.2

J = 4       # 4, 8
Z = 4       # 1, 4, 16
tc = 10     # ms

E_coop_cl_r = (1/J) * tc * P_cl_r
E_coop_sr_s = (1/(J*Z)) * tc * P_sr_s
E_coop_sr_r = ((J-1)/(J * Z)) * tc * P_sr_r

E_coop_sr_i = (1/J - 1/Z) * tc * P_sr_i
E_coop_cl_i = (1/Z - 1/J) * tc * P_cl_i


# Overall
E_over = P_cl_r * tc

# SLE
E_coop_sle = E_coop_cl_r + E_coop_sr_s + E_coop_sr_r
T_sle = ((1/J) + (1/Z)) * tc
G_sle = E_over/E_coop_sle

# PLE
fast_short = (1/J) > (1/Z)

if fast_short:
    E_coop_ple = E_coop_sle + E_coop_sr_i
    T_ple = (1/J) * tc
else:
    E_coop_ple = E_coop_sle + E_coop_cl_i
    T_ple = (1/Z) * tc
G_ple = E_over/E_coop_ple


print('Energy spent for No Coop case  = {}mWs'.format(E_over))
print('Energy spent for Coop SLE case = {}mWs'.format(E_coop_sle))
print('Energy spent for Coop PLE case = {}mWs'.format(E_coop_ple))

print('\nTime to transmit in No Coop case = {}ms'.format(tc))
print('Time to transmit in Coop SLE case  = {}ms'.format(T_sle))
print('Time to transmit in Coop PLE case  = {}ms'.format(T_ple))

print('\nSLE : Energy Gain, G = {}'.format(G_sle))
print('PLE : Energy Gain, G = {}'.format(G_ple))
